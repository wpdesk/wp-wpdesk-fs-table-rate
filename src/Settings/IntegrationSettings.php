<?php
/**
 * Integration settings.
 *
 * @package WPDesk\FS\TableRate\Settings
 */

namespace WPDesk\FS\TableRate\Settings;

/**
 * FS Integration settings on shipping method.
 */
interface IntegrationSettings {

	public function get_name();

}
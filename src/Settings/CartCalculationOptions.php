<?php
/**
 * Class CartCalculationOptions
 *
 * @package WPDesk\FS\TableRate\Settings
 */

namespace WPDesk\FS\TableRate\Settings;

use WPDesk\FS\TableRate\AbstractOptions;

/**
 * Can provide cart calculation options.
 */
class CartCalculationOptions extends AbstractOptions {

	const CART    = 'cart';
	const PACKAGE = 'package';

	/**
	 * @return array
	 */
	public function get_options() {
		return array(
			self::CART    => __( 'Cart value', 'wp-wpdesk-fs-table-rate' ),
			self::PACKAGE => __( 'Package value', 'wp-wpdesk-fs-table-rate' ),
		);
	}

}

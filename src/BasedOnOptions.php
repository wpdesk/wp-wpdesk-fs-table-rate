<?php
/**
 * Based on options.
 *
 * @package WPDesk\FS\TableRate
 */

namespace WPDesk\FS\TableRate;

/**
 * Can provide Based On options.
 */
class BasedOnOptions extends AbstractOptions {

	/**
	 * @return array
	 */
	public function get_options() {
		return apply_filters( 'flexible_shipping_method_rule_options_based_on', array(
			'none' 		=> __( 'None', 'wp-wpdesk-fs-table-rate' ),
			'value'  	=> __( 'Price', 'wp-wpdesk-fs-table-rate' ),
			'weight'  	=> __( 'Weight', 'wp-wpdesk-fs-table-rate' ),
		));
	}

}

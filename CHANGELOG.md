## [4.2.1] - 2022-05-24
### Fixed
- libraries

## [4.2.0] - 2022-04-21
### Changed
- upgrade wp-view libs

## [4.1.0] - 2021-10-28
### Changed
- method_default setting removed from Flexible Shipping Single shipping method
- assets enqueued only when needed

## [4.0.0] - 2021-09-01
### Changed
- updated libraries

## [3.4.0] - 2021-07-29
### Added
- prices_include_tax field in settings

## [3.3.1] - 2021-06-31
### Fixed
- Undefined index: method_calculation_method

## [3.3.0] - 2021-04-13
### Added
- cart calculation setting

## [3.2.4] - 2021-01-25
### Changed
- method enabled by default in debug message

## [3.2.3] - 2021-01-24
### Changed
- get_tax_status_translated method visibility to public

## [3.2.2] - 2021-01-24
### Fixed
- translations

## [3.2.0] - 2021-01-23
### Added
- tax status in settings

## [3.1.0] - 2021-01-18
### Changed
- method visible deprecated in MethodSettings
### Added  
- method visibility in MethodSettings

## [3.0.0] - 2020-11-18
### Changed
- new table rate rules settings

## [2.2.2] - 2020-10-01
### Fixed
- added jQuery to required JS

## [2.2.1] - 2020-09-22
### Added
- CalculationMethodOptions

## [2.2.0] - 2020-09-22
### Added
- AbstractOption

## [2.1.2] - 2020-09-22
### Fixed
- button labels in debug notices

## [2.1.1] - 2020-09-08
### Fixed
- missing translations

## [2.1.0] - 2020-09-08
### Added
- notice logger

## [2.0.1] - 2020-09-08
### Added
- rule settings

## [2.0.0] - 2020-09-08
### Changed
- new library version

## [1.0.1] - 2019-06-26
### Changed
- Moved html-order-add_shipping-metabox.php to wp-wpdesk-fs-shipment
- Moved html-pointer-message-fs-text.php html-pointer-message-fs-video.php to pointer view

## [1.0.0] - 2019-06-17
### Added
- Init
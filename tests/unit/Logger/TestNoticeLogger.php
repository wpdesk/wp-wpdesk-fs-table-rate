<?php

namespace Logger;

use WPDesk\FS\TableRate\Logger\NoticeLogger;
use PHPUnit\Framework\TestCase;

class TestNoticeLogger extends TestCase {

	public function setUp() {
		\WP_Mock::setUp();
	}

	public function tearDown() {
		\WP_Mock::tearDown();
	}

	public function test_disabled() {
		$notice_logger = new NoticeLogger( 'Test title', 'http://test.com', false );

		$notice_logger->log( 'debug', 'Test', array( 'section' => 'test' ) );

		\WP_Mock::userFunction( 'wc_has_notice', array( 'times' => 0 ) );

		\WP_Mock::userFunction( 'wc_add_notice', array( 'times' => 0 ) );

		$notice_logger->show_notice_if_enabled();

		$this->assertTrue( true );
	}

	public function test_enabled_with_one_section() {
		$notice_logger = new NoticeLogger( 'Test title', 'http://test.com', true );

		$notice_logger->debug( 'Test', array( 'section' => 'test' ) );
		$notice_logger->debug( 'Test 2', array( 'section' => 'test' ) );

		\WP_Mock::assertActionsCalled( 'flexible_shipping_debug_notice_added' );

		\WP_Mock::userFunction( 'wc_has_notice' )->times( 1 )->andReturn( false );

		\WP_Mock::userFunction( 'wc_add_notice' )->times( 1 )->with( 'FS Debug mode for <a href="http://test.com" target="_blank">Test title</a> shipping method.<br/>
<br/>
<div class="flexible-shipping-log">
	<button class="small show">Show test</button>
	<button class="small hide">Hide test</button>
	<button class="small clipboard">Copy test</button>
	<pre>Test
Test 2 </pre>
</div><br/><button class="small flexible-shipping-log-clipboard-all">Copy all data</button>', 'notice');

		$notice_logger->show_notice_if_enabled();

		$this->assertTrue( true );
	}

	public function test_enabled_with_two_sections() {
		$notice_logger = new NoticeLogger( 'Test title', 'http://test.com', true );

		$notice_logger->debug( 'Test', array( 'section' => 'test' ) );
		$notice_logger->debug( 'Test 2', array( 'section' => 'test 2' ) );

		\WP_Mock::userFunction( 'wc_has_notice' )->times( 1 )->andReturn( false );

		\WP_Mock::userFunction( 'wc_add_notice' )->times( 1 )->with( 'FS Debug mode for <a href="http://test.com" target="_blank">Test title</a> shipping method.<br/>
<br/>
<div class="flexible-shipping-log">
	<button class="small show">Show test</button>
	<button class="small hide">Hide test</button>
	<button class="small clipboard">Copy test</button>
	<pre>Test </pre>
</div><br/>
<div class="flexible-shipping-log">
	<button class="small show">Show test 2</button>
	<button class="small hide">Hide test 2</button>
	<button class="small clipboard">Copy test 2</button>
	<pre>Test 2 </pre>
</div><br/><button class="small flexible-shipping-log-clipboard-all">Copy all data</button>', 'notice' );

		$notice_logger->show_notice_if_enabled();

		$this->assertTrue( true );
	}

}
